#qsub -I -l 'nodes=1,mem=32gb,walltime=48:00:00'




#set the export folder (bids_folder) and raw folder.
bids_original='/project/4180000.24/germfree/bids'
bids_preprocessing='/project/4180000.24/germfree/preprocessing/step2'


#select which templates to use
template_dir='/groupshare/traaffneu/preclinimg/templates/DSURQE/'
template='DSURQE_40micron_average.nii.gz'
template_brain='DSURQE_40micron_brain.nii.gz'
template_mask='DSURQE_40micron_mask.nii.gz'
atlas='DSURQE_40micron_labels.nii.gz'

anatomical='T2w.nii.gz'

#to be adjusted as a function of SNR to improve brain masking step (higher is SNR is high and lower if low)
z=20    #for ex-vivo scans
#z=5      #for in-vivo scans


cd $bids_original

ls -d */*/anat | while read subject
do 
cd $subject



anat="$(ls *$anatomical)"
anat_noext="$(remove_ext $anat)"

output_dir=$bids_preprocessing'/'$subject
mkdir -p $output_dir
cp $anat $output_dir
cd $output_dir

cp $template_dir$template ./
cp $template_dir$template_brain ./
cp $template_dir$template_mask ./
cp $template_dir$atlas ./



echo "cd "$PWD > script.sh
echo "module load afni" >> script.sh
echo "module load singularity" >> script.sh

echo "3dAutobox -input "$anat" -prefix "$anat_noext"_crop.nii.gz" >> script.sh
echo "singularity run -B "${pwd}":/data --pwd data /opt/ANTs/2.4.0/ants-2.4.0.simg N4BiasFieldCorrection -d 3 -i "$anat_noext"_crop.nii.gz  -o "$anat_noext"_N4.nii.gz" >> script.sh  
echo "singularity run -B "${pwd}":/data --pwd data /opt/ANTs/2.4.0/ants-2.4.0.simg DenoiseImage -d 3 -i "$anat_noext"_N4.nii.gz  -o "$anat_noext"_dn.nii.gz" >> script.sh

echo "T=\$(fslstats "$anat_noext"_dn.nii.gz -p 100)" >> script.sh
echo "fslmaths "$anat_noext"_dn.nii.gz -div \$T -mul 100 "$anat_noext"_norm.nii.gz" >> script.sh

#brain masking step
echo "RATS_MM "$anat_noext"_norm.nii.gz "$anat_noext"_mask.nii.gz -t $z -v 400" >> script.sh 

echo "fslmaths "$anat_noext"_norm.nii.gz -mul "$anat_noext"_mask.nii.gz "$anat_noext"_brain.nii.gz" >> script.sh
echo "fslmaths "$anat_noext"_mask.nii.gz -dilF -dilF "$anat_noext"_mask_dil.nii.gz" >> script.sh

echo "fslmaths "$template_mask" -dilF -dilF dil_"$template_mask >> script.sh 


echo "singularity run -B "${pwd}":/data --pwd data /opt/ANTs/2.4.0/ants-2.4.0.simg antsRegistration --dimensionality 3 --float 0 --output [anat2temp, wrapped.nii.gz] --interpolation Linear --winsorize-image-intensities [0.005,0.995] --use-histogram-matching 0 --initial-moving-transform ["$template_brain","$anat_noext"_brain.nii.gz,1] --transform Rigid[0.1] --metric MI["$template_brain","$anat_noext"_brain.nii.gz,1,32,Regular,0.25] --convergence [1000x500x250x100,1e-6,10] --shrink-factors 8x4x2x1 --smoothing-sigmas 3x2x1x0vox --transform Affine[0.1] --metric MI["$template_brain","$anat_noext"_brain.nii.gz,1,32,Regular,0.25] --convergence [1000x500x250x100,1e-6,10] --shrink-factors 8x4x2x1 --smoothing-sigmas 3x2x1x0vox --transform SyN[0.1,3,0] --metric CC["$template","$anat_noext"_norm.nii.gz,1,4] --convergence [100x50x20x10,1e-6,10] --shrink-factors 8x4x2x1 --smoothing-sigmas 3x2x1x0vox -x [dil_"$template_mask","$anat_noext"_mask_dil.nii.gz] --verbose" >> script.sh


echo "singularity run -B "${pwd}":/data --pwd data /opt/ANTs/2.4.0/ants-2.4.0.simg WarpTimeSeriesImageMultiTransform 4 "$anat_noext"_norm.nii.gz "$anat_noext"_reg.nii.gz -R "$template" anat2temp1Warp.nii.gz anat2temp0GenericAffine.mat" >> script.sh

echo "singularity run -B "${pwd}":/data --pwd data /opt/ANTs/2.4.0/ants-2.4.0.simg CreateJacobianDeterminantImage 3 anat2temp1Warp.nii.gz "$anat_noext"_jac.nii.gz" >> script.sh


echo "singularity run  -B "${pwd}":/data --pwd data /opt/ANTs/2.4.0/ants-2.4.0.simg SmoothImage 3 "$anat_noext"_jac.nii.gz 0.15 "$anat_noext"_jacsmooth.nii.gz" >> script.sh


echo "mkdir -p reg" >> script.sh
echo "mv anat2* reg/" >> script.sh


echo "slicer "$anat_noext"_reg.nii.gz "$template" -s 2 -x 0.35 sla.png -x 0.45 slb.png -x 0.55 slc.png -x 0.65 sld.png -y 0.35 sle.png -y 0.45 slf.png -y 0.55 slg.png -y 0.65 slh.png -z 0.35 sli.png -z 0.45 slj.png -z 0.55 slk.png -z 0.65 sll.png ; pngappend sla.png + slb.png + slc.png + sld.png + sle.png + slf.png + slg.png + slh.png + sli.png + slj.png + slk.png + sll.png highres2standard1.png ; slicer "$template" "$anat_noext"_reg.nii.gz -s 2 -x 0.35 sla.png -x 0.45 slb.png -x 0.55 slc.png -x 0.65 sld.png -y 0.35 sle.png -y 0.45 slf.png -y 0.55 slg.png -y 0.65 slh.png -z 0.35 sli.png -z 0.45 slj.png -z 0.55 slk.png -z 0.65 sll.png ; pngappend sla.png + slb.png + slc.png + sld.png + sle.png + slf.png + slg.png + slh.png + sli.png + slj.png + slk.png + sll.png highres2standard2.png ; pngappend highres2standard1.png - highres2standard2.png anat2temp.png; rm -f sl?.png highres2standard2.png; rm highres2standard1.png" >> script.sh

echo "fslmeants -i "$anat_noext"_jac.nii.gz -o "$anat_noext"_roi.txt -m "$template_mask" --label="$atlas >> script.sh

qsub -l 'nodes=1,mem=24gb,walltime=08:00:00' ${PWD}/script.sh


cd $bids_original
done 
