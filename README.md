Germfee analysis
================

## Preprocessing

Preprocessing of the data was performed using ANTs with this
[script]('assets/anat_template.sh')

## Load environement

``` r
library(tidyverse)
```

    ## Warning: replacing previous import 'lifecycle::last_warnings' by
    ## 'rlang::last_warnings' when loading 'pillar'

    ## Warning: replacing previous import 'lifecycle::last_warnings' by
    ## 'rlang::last_warnings' when loading 'tibble'

    ## Warning: replacing previous import 'lifecycle::last_warnings' by
    ## 'rlang::last_warnings' when loading 'hms'

## Load the data onto a table.

``` r
roi_list<-read_csv('assets/roi_label_clean.csv')
```

    ## 
    ## ── Column specification ────────────────────────────────────────────────────────
    ## cols(
    ##   .default = col_double(),
    ##   dsurqe_struct = col_character(),
    ##   abi_struct = col_character(),
    ##   tissue = col_character(),
    ##   hemisphere = col_character()
    ## )
    ## ℹ Use `spec()` for the full column specifications.

``` r
data_dir<-'/project/4180000.24/germfree/preprocessing/step2'
file_list<-list.files(pattern="*roi.txt",path = data_dir, recursive = TRUE,full.names = TRUE)

for(i in file_list){
  ID<-i %>% basename() %>% str_split("_",simplify = TRUE) %>% .[1] %>% str_split('-',simplify = TRUE) %>% .[2]
  tmp<-read_table(i,col_names = FALSE) %>% select(roi_list$label) %>% slice(1) %>% unlist(., use.names=FALSE)
  tmp_tib<-tibble(!!ID := tmp)
  roi_list<-roi_list %>% bind_cols(tmp_tib)
}
```

    ## 
    ## ── Column specification ────────────────────────────────────────────────────────
    ## cols(
    ##   .default = col_double()
    ## )
    ## ℹ Use `spec()` for the full column specifications.

    ## New names:
    ## * jgrGermFreeConV1 -> jgrGermFreeConV1...6
    ## * jgrGermFreeConV1 -> jgrGermFreeConV1...21

    ## 
    ## ── Column specification ────────────────────────────────────────────────────────
    ## cols(
    ##   .default = col_double()
    ## )
    ## ℹ Use `spec()` for the full column specifications.

    ## New names:
    ## * jgrGermFreeCONV2 -> jgrGermFreeCONV2...7
    ## * jgrGermFreeCONV2 -> jgrGermFreeCONV2...22

    ## 
    ## ── Column specification ────────────────────────────────────────────────────────
    ## cols(
    ##   .default = col_double()
    ## )
    ## ℹ Use `spec()` for the full column specifications.

    ## New names:
    ## * jgrGermFreeConV3 -> jgrGermFreeConV3...8
    ## * jgrGermFreeConV3 -> jgrGermFreeConV3...23

    ## 
    ## ── Column specification ────────────────────────────────────────────────────────
    ## cols(
    ##   .default = col_double()
    ## )
    ## ℹ Use `spec()` for the full column specifications.

    ## New names:
    ## * jgrGermFreeConV4 -> jgrGermFreeConV4...9
    ## * jgrGermFreeConV4 -> jgrGermFreeConV4...24

    ## 
    ## ── Column specification ────────────────────────────────────────────────────────
    ## cols(
    ##   .default = col_double()
    ## )
    ## ℹ Use `spec()` for the full column specifications.

    ## New names:
    ## * jgrGermfreeConV5 -> jgrGermfreeConV5...10
    ## * jgrGermfreeConV5 -> jgrGermfreeConV5...25

    ## 
    ## ── Column specification ────────────────────────────────────────────────────────
    ## cols(
    ##   .default = col_double()
    ## )
    ## ℹ Use `spec()` for the full column specifications.

    ## New names:
    ## * jgrGermFreeGF1 -> jgrGermFreeGF1...11
    ## * jgrGermFreeGF1 -> jgrGermFreeGF1...26

    ## 
    ## ── Column specification ────────────────────────────────────────────────────────
    ## cols(
    ##   .default = col_double()
    ## )
    ## ℹ Use `spec()` for the full column specifications.

    ## New names:
    ## * jgrGermFreeGF2 -> jgrGermFreeGF2...12
    ## * jgrGermFreeGF2 -> jgrGermFreeGF2...27

    ## 
    ## ── Column specification ────────────────────────────────────────────────────────
    ## cols(
    ##   .default = col_double()
    ## )
    ## ℹ Use `spec()` for the full column specifications.

    ## New names:
    ## * jgrGermFreeGF3 -> jgrGermFreeGF3...13
    ## * jgrGermFreeGF3 -> jgrGermFreeGF3...28

    ## 
    ## ── Column specification ────────────────────────────────────────────────────────
    ## cols(
    ##   .default = col_double()
    ## )
    ## ℹ Use `spec()` for the full column specifications.

    ## New names:
    ## * jgrGermFreeGF4 -> jgrGermFreeGF4...14
    ## * jgrGermFreeGF4 -> jgrGermFreeGF4...29

    ## 
    ## ── Column specification ────────────────────────────────────────────────────────
    ## cols(
    ##   .default = col_double()
    ## )
    ## ℹ Use `spec()` for the full column specifications.

    ## New names:
    ## * jgrGermfreeGF5 -> jgrGermfreeGF5...15
    ## * jgrGermfreeGF5 -> jgrGermfreeGF5...30

    ## 
    ## ── Column specification ────────────────────────────────────────────────────────
    ## cols(
    ##   .default = col_double()
    ## )
    ## ℹ Use `spec()` for the full column specifications.

    ## New names:
    ## * jgrGermFreeSPF1 -> jgrGermFreeSPF1...16
    ## * jgrGermFreeSPF1 -> jgrGermFreeSPF1...31

    ## 
    ## ── Column specification ────────────────────────────────────────────────────────
    ## cols(
    ##   .default = col_double()
    ## )
    ## ℹ Use `spec()` for the full column specifications.

    ## New names:
    ## * jgrGermFreeSPF2 -> jgrGermFreeSPF2...17
    ## * jgrGermFreeSPF2 -> jgrGermFreeSPF2...32

    ## 
    ## ── Column specification ────────────────────────────────────────────────────────
    ## cols(
    ##   .default = col_double()
    ## )
    ## ℹ Use `spec()` for the full column specifications.

    ## New names:
    ## * jgrGermFreeSPF3 -> jgrGermFreeSPF3...18
    ## * jgrGermFreeSPF3 -> jgrGermFreeSPF3...33

    ## 
    ## ── Column specification ────────────────────────────────────────────────────────
    ## cols(
    ##   .default = col_double()
    ## )
    ## ℹ Use `spec()` for the full column specifications.

    ## New names:
    ## * jgrGermFreeSPF4 -> jgrGermFreeSPF4...19
    ## * jgrGermFreeSPF4 -> jgrGermFreeSPF4...34

    ## 
    ## ── Column specification ────────────────────────────────────────────────────────
    ## cols(
    ##   .default = col_double()
    ## )
    ## ℹ Use `spec()` for the full column specifications.

    ## New names:
    ## * jgrGermFreeSPF5 -> jgrGermFreeSPF5...20
    ## * jgrGermFreeSPF5 -> jgrGermFreeSPF5...35

``` r
write_csv(roi_list,'assets/roi_label_clean.csv')
```
